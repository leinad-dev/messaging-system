<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/migrate-fresh', function () {
    
    Artisan::call('migrate:fresh');
    
    Artisan::call('db:seed');
    
    Artisan::call('config:cache');
    
    Artisan::call('config:clear');
    
    Artisan::call('cache:clear');
    
    Artisan::call('route:clear');
    
    Artisan::call('view:clear');
    
    Artisan::call('clear-compiled');
    
    return "OK.";
});

Route::get('/clear-cache', function () {
    
    Artisan::call('config:cache');
    
    Artisan::call('config:clear');
    
    Artisan::call('cache:clear');
    
    Artisan::call('route:clear');
    
    Artisan::call('view:clear');
    
    Artisan::call('clear-compiled');
    
    return "OK.";
});

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/conversation', 'ConversationController@index')->name('conversation.index');

Route::get('/conversation/{user}', 'ConversationController@index')->name('conversation.show');

