import Vue from "vue";
import Vuex from 'vuex'
const axios = require('axios');

Vue.use(Vuex);

export default new Vuex.Store({
    strict: true,
    state: {
        currentUser: {},
        conversationUsers: {},
    },
    getters: {
        currentUser: function (state) {
            return state.currentUser
        },

        conversationUsers: function (state) {
            return state.conversationUsers
        },

        conversationMessages: function (state) {
            return function (userSlug) {
                let currentConversationUser = state.conversationUsers[userSlug] || {};

                if (currentConversationUser && currentConversationUser.messages) {
                    return state.conversationUsers[userSlug].messages
                }

                return []
            }
        },

        conversationUser: function (state) {
            return function (userSlug) {
                return state.conversationUsers[userSlug] || {}
            }
        }
    },
    mutations: {
        updateConversationUsers: function (state, {currentUser, conversationUsers}) {
            conversationUsers.forEach(function (conversationUser) {
                let currentConversationUser = state.conversationUsers[conversationUser.slug] || {};
                currentConversationUser = {...currentConversationUser, ...conversationUser};

                state.conversationUsers = {...state.conversationUsers, ...{[conversationUser.slug]: currentConversationUser}};
                state.currentUser = currentUser;
            });
        },
        updateConversationMessages: function (state, {userSlug, conversationMessages}) {
            let currentConversationUser = state.conversationUsers[userSlug] || {};
            currentConversationUser.messages = conversationMessages;
            currentConversationUser.messagesLoaded = true;

            state.conversationUsers = {...state.conversationUsers, ...{[userSlug]: currentConversationUser}}
        }
    },
    actions: {
        loadConversationUsers: function (context) {
            axios.get('/api/conversation-users')
                .then(function (response) {
                    context.commit('updateConversationUsers', {
                        currentUser: response.data.currentUser,
                        conversationUsers: response.data.conversationUsers
                    })
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
                .then(function () {
                    // always executed
                });
        },

        loadMessages(context, userSlug) {
            if (!context.getters.conversationUser(userSlug).messagesLoaded) {
                axios.get('/api/conversation-users/' + userSlug)
                    .then(function (response) {
                        context.commit('updateConversationMessages', {
                            userSlug: userSlug,
                            conversationMessages: response.data.conversationMessages
                        })
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    })
                    .then(function () {
                        // always executed
                    });
            }
        },

        sendMessage(context, {userSlug, content}) {
            axios.post('/api/conversation-users/' + userSlug, {
                content: content
            })
                .then(function (response) {
                    // context.commit('updateConversationMessages', {
                    //     userSlug: userSlug,
                    //     conversationMessages: response.data.conversationMessages
                    // })
                    console.log(response);
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
                .then(function () {
                    // always executed
                });
        }
    }
})