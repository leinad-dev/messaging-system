require('axios');

require('./bootstrap');

import Vue from 'vue'

import store from './store'

import VueRouter from 'vue-router'

import ConversationUsersComponent from './components/ConversationUsersComponent'

import ConversationMessageComponent from './components/ConversationMessageComponent'

Vue.use(VueRouter);

const routes = [
    {path: '/'},
    {path: '/:slug', component: ConversationMessageComponent, name: 'show-conversation'},
];

let appDiv = document.querySelector('#app');

const router = new VueRouter({
    mode: 'history',
    routes,
    base: appDiv.getAttribute('data-base')
});

const app = new Vue({
    el: '#app',
    components: {
        'conversation-users': ConversationUsersComponent
    },
    store,
    router
});