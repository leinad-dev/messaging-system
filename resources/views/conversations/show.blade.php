@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-3">
                @include('conversations._users', ['users' => $users])
            </div>

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header"><strong>{{ $user->name }}</strong></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @php
                            $previousDate = now();
                        @endphp

                        @foreach($messages as $message)
                            @if($message->created_at->diffInDays($previousDate) !== 0)

                                @if($message->created_at->diffInDays(now()) == 0)
                                    <p style="text-align: center;">
                                        <strong>Today</strong>
                                    </p>
                                @elseif($message->created_at->diffInDays(now()) == 1)
                                    <p style="text-align: center;">
                                        <strong>Yesterday</strong>
                                    </p>
                                @else
                                    <p style="text-align: center;">
                                        <strong>{{ $message->created_at->format('F d') }}</strong>
                                    </p>
                                @endif

                                @php
                                    $previousDate = $message->created_at;
                                @endphp
                            @endif

                            <div class="row">
                                <div class="col-sm-10 {{ $message->from_id === auth()->id() ? 'offset-sm-2 text-right' : null }}">
                                    <p>
                                        <strong>{!! nl2br(e($message->content)) !!}</strong> <span
                                                style="font-size: 13px">{{ $message->created_at->format('H:i') }}</span>
                                    </p>
                                </div>
                            </div>

                            @if(!$loop->last)
                                <hr>
                            @endif
                        @endforeach
                        <br>

                        <form action="{{ route('conversation.store', $user) }}" method="post">
                            {{ csrf_field() }}

                            <div class="form-group" id="textarea">
                                <textarea name="content" class="form-control" required></textarea>
                            </div>

                            <button class="btn btn-light btn-lg" type="submit">Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection