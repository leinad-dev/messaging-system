<div class="card">
    <div class="card-header">Users</div>
    <div class="card-body">
        <div class="list-group">
            @foreach($users as $user)
                <a href="{{ route('conversation.show', $user) }}#textarea"
                   class="list-group-item">{{ $user->name }}

                    @if($user->messages_where_from_count)
                        <span class="badge badge-pill badge-success">
                            {{ $user->messages_where_from_count }}
                        </span>
                    @endif
                </a>
            @endforeach
        </div>
    </div>
</div>