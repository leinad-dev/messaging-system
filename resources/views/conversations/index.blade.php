@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-3">
                <conversation-users></conversation-users>
            </div>

            <div class="col-md-9">
                <router-view></router-view>
            </div>
        </div>
    </div>
@endsection
