<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'content' => $this->content,
            'to_id' => $this->to_id,
            'from_id' => $this->from_id,
            'read_at' => $this->read_at == null ?: $this->read_at->format('H:i'),
            'created_at' => $this->created_at->format('H:i'),
        ];
    }
}
