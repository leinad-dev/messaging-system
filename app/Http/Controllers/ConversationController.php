<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ConversationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
//        $users = $this->getOtherUsers();
        
        return view('conversations.index', compact('users'));
    }
    
    public function show(User $user)
    {
        $messages = Message::whereIn('from_id', [auth()->id(), $user->id])
            ->whereIn('to_id', [auth()->id(), $user->id])
            ->orderBy('created_at')
            ->get();
        
        $unReadMessages = $messages->where('read_at', '=', null)
            ->where('to_id', "=", auth()->id())
            ->map(function ($message) {
                return $message->id;
            });
        
        if ($unReadMessages->isNotEmpty()) {
            Message::whereIn('id', $unReadMessages->toArray())->update(['read_at' => now()]);
        }
        
        $users = $this->getOtherUsers();
        
        return view('conversations.show', compact('users', 'user', 'messages'));
    }
    
    public function store(Request $request, User $user)
    {
        $validator = validator($request->all(), ['content' => 'required|string']);
        if ($validator->fails()) {
            return back()->with($validator)->withInput();
        }
        
        /** @var User $authUser */
        $authUser = auth()->user();
        $authUser->messagesWhereFrom()->create([
            'content' => $request->input('content'),
            'to_id' => $user->id
        ]);
        
        return redirect(route('conversation.show', $user));
    }
    
    private function getOtherUsers(): Collection
    {
        $users = User::withCount([
            "messagesWhereFrom" => function ($query) {
                return $query->whereNull('read_at')->whereToId(auth()->id());
            }
        ])->where('id', '!=', auth()->id())->get(['id', 'name', 'slug']);
        
        return $users;
    }
}
