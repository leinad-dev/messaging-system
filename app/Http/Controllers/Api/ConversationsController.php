<?php
/**
 * Created by PhpStorm.
 * User: leinad
 * Date: 10/17/18
 * Time: 2:47 PM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Resources\MessageResource;
use App\Http\Resources\UserResource;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;

class ConversationsController extends Controller
{
    function users(Request $request)
    {
        $users = User::withCount([
            "messagesWhereFrom" => function ($query) use ($request) {
                return $query->whereNull('read_at')->whereToId($request->user()->id);
            }
        ])->where('id', '!=', $request->user()->id)->get(['id', 'name', 'slug']);
        
        return response()->json([
            'conversationUsers' => UserResource::collection($users),
            'currentUser' => new UserResource($request->user())
        ]);
    }
    
    function messages(Request $request, User $user)
    {
        $messages = Message::whereIn('from_id', [$request->user()->id, $user->id])
            ->whereIn('to_id', [$request->user()->id, $user->id])
            ->orderBy('created_at')
            ->get();
        
        return response()->json(['conversationMessages' => MessageResource::collection($messages)]);
    }
    
    function sendMessages(Request $request, User $user)
    {
        $validator = validator($request->all(), ['content' => 'required|string']);
        if ($validator->fails()) {
            return response()->json(['message' => join(" ", $validator->errors()->get('content'))], 400);
        }
        
        /** @var User $authUser */
        $authUser = $request->user();
        $message = $authUser->messagesWhereFrom()->create([
            'content' => $request->input('content'),
            'to_id' => $user->id
        ]);
        
        return response()->json(['message' => new MessageResource($message)]);
    }
}