<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Message
 *
 * @property int $id
 * @property int $from_id
 * @property int $to_id
 * @property string $content
 * @property \Illuminate\Support\Carbon|null $read_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $from
 * @property-read \App\Models\User $to
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereFromId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereReadAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereToId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Message extends Model
{
    protected $dates = ['read_at'];
    
    protected $guarded = [];
    
    public function from()
    {
        return $this->belongsTo(User::class, 'from_id', 'id');
    }
    
    public function to()
    {
        return $this->belongsTo(User::class, 'to_id', 'id');
    }
}
